Tout ce que je sais, c'est que je ne sais rien, tandis que les autres croient savoir ce qu'ils ne savent pas.
Existe-t-il pour l'homme un bien plus précieux que la Santé ?
Je ne suis ni Athénien, ni Grec, mais un citoyen du monde.
Connais-toi toi-même.
L'homme doit s'élever au-dessus de la Terre - aux limites de l'atmosphère et au-delà - ainsi seulement pourra-t-il comprendre tout à fait le monde dans lequel il vit.
 La plus intelligente est celle qui sait qu'elle ne sait pas.
Les gens qu'on interroge, pourvu qu'on les interroge bien, trouvent d'eux-mêmes les bonnes réponses.
